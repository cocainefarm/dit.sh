#!/bin/bash

# $1 = clone 
# $2 = url
# $3 = dest 

URLREGEX="https?:\/\/(www\.)?([-a-zA-Z1-9@:%._\+~#=]{2,256}\.[a-z]{2,6})\b\/([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)|[a-zA-Z0-9]*@([-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6})\b:([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)"

if [[ -z $DITSH_URL ]]
 then DITSH_URL="gitlab.com"
fi

if [[ -z $DITSH_BASE ]]
  then 
    echo "ERROR: Base directory not set"
    exit 2
  else DITSH_BASE=${DITSH_BASE%"/"}
fi

if [[ -n $3 ]]; then
    git clone "$2" "$3"
elif [[ $2 =~ $URLREGEX ]]; then
    host="${BASH_REMATCH[4]}${BASH_REMATCH[2]}"
    path="${BASH_REMATCH[5]}${BASH_REMATCH[3]}"
    dest="${DITSH_BASE}/${host}/${path%".git"}"

    mkdir -p "${dest}"
    git clone "$2" "${dest}"
fi
