# dit.sh

dit.sh is a small tool aiding in cloning and organising repositories based on the site and group/person they are owned by

dit.sh will clone into a directory structure based on `$BASE/$SITE/$PERSON/$REPO` and it gets these informations purely from the url given to it.

it will also take a short identifier like `ejectedspace/dit.sh` and clone the repo from a preset site

## Install

put `dit.sh` whereever you wish and add this snippet pointing to it to your `bashrc` or `zshrc`:

```
function git() {
  if [[ "$1" == "clone" ]]; then
    command $HOME/.local/bin/dit.sh "$@"
  else
    command git "$@"
  fi
}

export DITSH_URL=$HOME/repo
```

## Settings

Environment Variables:

* `$DITSH_BASE`: base directory to clone repos to. required to set.
* `$DITSH_URL`: default url to use with short clone syntax. (default "gitlab.com")
* `$DITSH_SSH`: whether or not to use SSH. (bool default "true")
